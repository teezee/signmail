#!/bin/bash
###
# Copyright (c) 2016 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# remove passwort from smime certificate
# inspired by: http://serverfault.com/questions/515833/how-to-remove-private-key-password-from-pkcs12-container
###

set -euf -o pipefail
E_BADARGS=1
E_BADFILE=2

usage() {
	echo "Usage: $(basename "$0") <PKCS12_File>" 
}

# handle errors
if [ $# -ne 1 ]; then
	usage;
	exit "$E_BADARGS";
fi

if [ ! -f "$1" ]; then
	echo "File $1 not found. Exiting."
	usage;
	exit "$E_BADFILE";
fi

# handle infile
infile=$1
fpath="${infile%/*}"
fname="$(basename -- "$infile")"
fext="${fname##*.}"
fname="${fname%.*}"

# get password
read -r -s -p "Enter Import Password: " PASSWORD
#TemporaryPassword=$(pwgen 8 1)

# 1. extract the certificate:
openssl pkcs12 -clcerts -nokeys -in "$infile" -out "${fname}".crt -password pass:"$PASSWORD" -passin pass:"$PASSWORD"

# 2. extract the CA key:
openssl pkcs12 -cacerts -nokeys -in "$infile" -out ca-cert.ca -password pass:"$PASSWORD" -passin pass:"$PASSWORD"

# 3. extract the private key:
#openssl pkcs12 -nocerts -in "$infile" -out private.key -password pass:"$PASSWORD" -passin pass:"$PASSWORD" -passout pass:"$TemporaryPassword"
openssl pkcs12 -nocerts -in "$infile" -out "${fname}".key -password pass:"$PASSWORD" -passin pass:"$PASSWORD" -passout pass:"$PASSWORD"

# 4. remove passphrase:
#openssl rsa -in private.key -out "NewKeyFile.key" -passin pass:"$TemporaryPassword"
openssl rsa -in "${fname}".key -out "${fname}_no-pass.key" -passin pass:"$PASSWORD"

# 5. put things together:
outpem="${fpath}/${fname}_no-pass.pem"
#cat "NewKeyFile.key" > "$outpem"
#cat "certificate.crt" >> "$outpem"
cat "${fname}_no-pass.key" >> "$outpem"
cat "${fname}.crt" >> "$outpem"
cat "ca-cert.ca" >> "$outpem"

#create the new pkcs12 file:
outp12="${fpath}/${fname}_empty-password.${fext}"
echo "Press Enter twice to set empty password"
openssl pkcs12 -export -nodes -CAfile ca-cert.ca -in "$outpem" -out "$outp12"

#cleaning
#rm -v NewKeyFile.key ca-cert.ca certificate.crt private.key
rm -v ca-cert.ca "${fname}_no-pass.key" "${fname}.key" 
